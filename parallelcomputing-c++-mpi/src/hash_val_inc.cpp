/*
Call func3 directly, with input string of 64 characters.
This call will return the doubled hex string.
*/
#include <string>
#include <iostream>
#include <cstring>
#include <array>
#include <map>
#include <math.h>
#include <sstream>
#include <immintrin.h>

std::string func2(std::string);
std::string func3(std::string);

// int main(){
//     std::string s1 = func3("0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc");
//     std::cout << s1;
//     return 0;
// }

std::string func2(std::string val){
    std::map<char, int> hex_vals;
    hex_vals['a'] = 10;
    hex_vals['b'] = 11;
    hex_vals['c'] = 12;
    hex_vals['d'] = 13;
    hex_vals['e'] = 14;
    hex_vals['f'] = 15;
    int base_value = 16;
    char arr[val.length()];
    strcpy(arr, val.c_str());
    long int sum = 0;
    for (int i = 0; i < sizeof(arr); i++)
    {   
        if(hex_vals.find(arr[i]) != hex_vals.end()){
            sum += hex_vals[arr[i]]*(pow(base_value,i))*2;
        }
        else
        {
            sum += i*(pow(base_value,i))*2;
        }
    }
    std::stringstream ss;
    ss << std::hex << sum;
    std::string newz = ss.str();
    return newz;
} 

std::string func3(std::string val){
    std::string divided[16];
    std::string sums[16];
    int j = 0;
    for (int i = 0; i < val.length(); i+=4)
    {
        divided[j] = val.substr(i, 4);
        j++;
    }
    for (int i = 0; i < 16; i++)
    {
        sums[i] = func2(divided[i]);
    }
    for (int i = 15; i > 0; i--)
    {
        if(sums[i].length() > 4){
            unsigned long long int x = 0;
            unsigned long long int y = 0;
            std::stringstream ss;
            std::stringstream ss1; 
            ss << std::hex << sums[i].substr(0,1); 
            ss >> x;
            sums[i] = sums[i].substr(1,4);
            ss1 << std::hex << sums[i-1]; 
            ss1 >> y;
            std::stringstream ssf;
            ssf << std::hex << (x+y);
            std::string newz = ssf.str();        
            sums[i-1] =  newz;
        }
    }
    std::string retVal = "";
    for (int i = 0; i < 15; i++)
    {
        retVal = retVal + sums[i] ;    
    }
    return retVal;
}
