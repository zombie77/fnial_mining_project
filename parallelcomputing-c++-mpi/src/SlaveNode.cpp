#include <iostream>
#include <thread>
#include <mpi/mpi.h>

#include "sha256.h"

using std::thread;
using std::string;
using std::cout;

class SlaveNode
{
private:
    //useless for now
    thread* worker;
public:
    SlaveNode(){};
    SlaveNode(int);
    ~SlaveNode();

    //this function use openmp
    int* calculateNonce(const int lowerIndex,const int upperIndex,
     const string& blockHash, const string& targetHash, bool&stopFlag);
};

SlaveNode::SlaveNode(int threadCount)
{
    //my plan for this constructor was to use 
    //number of thread as user requested, but
    //openmp decides the thread count at compile time
}

SlaveNode::~SlaveNode()
{
    delete[] worker;
}

int* SlaveNode::calculateNonce(const int lowerIndex,const int upperIndex,
    const string& blockHash, const string& targetHash, bool& stopFlag){
    int* nonce = nullptr;
    #pragma omp parallel
    {
        SHA256 sha256Generator;
        #pragma omp for
        //once thr nunce pointer is not empty, it exits
        for(int i = lowerIndex;i <= upperIndex && nonce == nullptr; i++){
            string str = blockHash + std::to_string(i);
            //two rounds of hashing
            string tempHash = sha256Generator(sha256Generator(str));
            //if the new sha has more leading 0s return the nonce
            if(targetHash.compare(tempHash) >= 0){
                cout<<"result hash = "<<tempHash<<'\n';

                //record only one result
                nonce = new int;
                *nonce = i;
            }
        }
    }
    return nonce;
}


string myTestTarget(){
    char target[65];
    for(int i=0;i<64;i++)
        target[i] = '1';
    target[64] = '\0';
    string target_str(target);
    cout<<target_str<<'\n';
    return target_str;
}

/**
 * OpenMP is designed for programs where you want a fixed number of
 * threads, and you always want the threads to be consuming CPU cycles.
 * – cannot arbitrarily start/stop threads
 * – cannot put threads to sleep and wake them up later
 * 
 * so if we can implement our own thread pool, we might get performence boost
 */
int main(int argc, char *argv[])
{
    const int threadCount = 4;
    int lowerIndex, upperIndex;
    SlaveNode* slave = new SlaveNode();

    //init slave
    MPI_Init(&argc,&argv);

    //recive lower and upper index
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    cout<<"slave "<<rank<<" running, waiting for message\n";
    int rangeBuffer[2];
    MPI_Recv(rangeBuffer, 2, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("slave %d recived (%d,%d)\n", rank, rangeBuffer[0], rangeBuffer[1]);
    lowerIndex = rangeBuffer[0]; upperIndex = rangeBuffer[1];

    bool terminateFlag;
    MPI_Bcast(&terminateFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);
    int runCount = 0;
    while (!terminateFlag)
    {
        //recive target
        char hashBuffer[65];
        MPI_Bcast(&hashBuffer, 65, MPI_CHAR, 0, MPI_COMM_WORLD);
        string target_str(hashBuffer);

        //recive blockHash
        MPI_Bcast(&hashBuffer, 65, MPI_CHAR, 0, MPI_COMM_WORLD);
        string blockHash(hashBuffer);

        //after calling the bcast, the rank value was reset to 0 idk why
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        printf("slave %d recived target hash: %s\n", rank, target_str.c_str());
        printf("slave %d recived block hash: %s\n", rank, blockHash.c_str());

        bool stopFlag = false;
        //TODO seperate the calculation on another thread so we may recive the stop singnal from master
        int* result = slave->calculateNonce(lowerIndex, upperIndex, blockHash, target_str, stopFlag);
        if(result != nullptr){
            cout<<"result i = "<<*result<<'\n';
            //send result if any
            MPI_Send(result, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            delete result;
        }
        runCount++;
        //check termination flag after each round
        MPI_Bcast(&terminateFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
    cout<<"total run count = "<<runCount <<std::endl;
    MPI_Finalize();
    return 0;
}
