#include <iostream>
#include <string>
#include <chrono>
#include <unistd.h>
#include <vector>
#include <mpi/mpi.h>
#include <iomanip>
#include <sstream>

#include "sha256.h"

using std::cout;
using std::string;
using std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::vector;
using std::pair;

bool init(int argc, char** argv,string &initBlock, string& initTargetHash);
bool initMPI(int argc, char** argv);
void calcluateSearchRange(int slaveCount, int** rangePairs);
string myTestTarget(int leadingZeros);
void broadcastHash(string hashStr);
int receiveNonce();
unsigned long long* hexStrTo32bitsNumberSet(const string& bigHexNumber, int &size);
string bigHexNumberMultiple(const string &bigHexNumber, int multiplier);
string bigHexNumberDivision(const string &bigHexNumber, int divisor);

//turn any even length hex string into a byte vector
//sha256 value should be a 32byte value
vector<unsigned char> *HexToBytes(const string &hex) {
    vector<unsigned char> *bytes = new vector<unsigned char>();
    for (unsigned int i = 0; i < hex.length(); i += 2) {
        string byteString = hex.substr(i, 2);
        unsigned char byte = (unsigned char)strtol(byteString.c_str(), NULL, 16);
        bytes->push_back(byte);
    }
    return bytes;
}

/**
 * This progamme will look for a nonce that satisfy target value.
 * This process will loop for several times specified by maxBlockId
 * 
 * TODO: try MPI_Ssend may perform better
 */
int main(int argc, char *argv[])
{
    //init param
    SHA256 sha256Generator;
    string initBlock;
    string blockHash;
    string targetHash;

    //how many blocks will this programme generates
    int currentBlockId = 0;
    const int maxBlockId = 5;
    int nonce;
    const double avgBlockGenerationTimeInSec = 30.0f;
    string initBlockHash;

    //if init failed return code 1
    if(!init(argc, argv, initBlock, targetHash)) return 1;

    //test run
    // initBlock = "this is my test run\n";
    // targetHash = myTestTarget(1);

    //init the master
    initMPI(argc, argv);

    //reloaded operator()
    blockHash = sha256Generator(initBlock);
    cout<<("Initial Block Value:  " + initBlock + "\n");
    cout<<("Initial Block Hash:  " + blockHash + "\n");
    cout<<("Initial Target Hash: " + targetHash + "\n");

    //init slaves
    //in current version all the slaves has constant search range
    //to accommodate nodes joining and leaving the master should be able to adjust it accordingly
    //get world slave count
    int slaveCount;
    MPI_Comm_size(MPI_COMM_WORLD, &slaveCount);
    --slaveCount;//world size - 1(master)

    //test run
    slaveCount = 2;

    //send nonce range, block hash and target hash
    //the last one node in rank carries a little bit more calculation
    //in the worest case it would be (slaveCount - 1)
    int** rangePairs = new int*[slaveCount];
    for(int i=0;i<slaveCount;i++){
        rangePairs[i] = new int[2];
    }
    calcluateSearchRange(slaveCount, rangePairs);

    // test devison of range output
    for(int i=0;i<slaveCount;i++){
        printf("slave %d : range (%d,%d)\n", i+1, rangePairs[i][0], rangePairs[i][1]);
    }

    //send range value
    cout<<"master try sending search range\n";
    for(int i=1; i<=slaveCount;i++)
        MPI_Send(rangePairs[i-1], 2, MPI_INT, i, 0, MPI_COMM_WORLD);

    bool terminationFlag = false;
    while (currentBlockId < maxBlockId)
    {
        auto start = steady_clock::now();

        //send termination flag
        MPI_Bcast(&terminationFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);
        //send targetHash
        printf("master: try send target hash, round %d\n", currentBlockId);
        broadcastHash(targetHash);
        broadcastHash(blockHash);

        //recive nonce
        nonce = receiveNonce();
        cout<<"master recived: "<<nonce<<std::endl;

        //TODO: Bcast stop all slaves
        auto end = steady_clock::now();
        double elapsedTime = duration_cast<milliseconds>(end - start).count();
        cout<< "time elapsed: " << elapsedTime << " round = " <<currentBlockId<< std::endl;

        //clculate new target hash
        if(elapsedTime/1000 < avgBlockGenerationTimeInSec){
            targetHash = bigHexNumberDivision(targetHash,2);
        }
        else{
            targetHash = bigHexNumberMultiple(targetHash,2);
        }
        currentBlockId++;

        //generate new block hash
        blockHash = sha256Generator(blockHash + "|" + std::to_string(nonce));
    }
    terminationFlag = true;
    MPI_Bcast(&terminationFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);

    for(int i;i<slaveCount;i++){
        delete[] rangePairs[i];
    }
    delete[] rangePairs;
    MPI_Finalize();
}

bool init(int argc, char** argv,string &initBlock, string& initTargetHash){
    if(argc > 0){
        if(argc == 3){
            initBlock = string(argv[1]);
            initTargetHash = string(argv[2]);
        }
        else{
            cout<<"wrong input\n";
            return false;
        }
    }
    return true;
}

bool initMPI(int argc, char** argv){
    MPI_Init(&argc, &argv);
    return true;
}

void calcluateSearchRange(int slaveCount, int** rangePairs){
    //total number of possible nonce is (2 * max int)
    int currentIndex = INT32_MIN, stepLength = INT32_MAX/(slaveCount/2);
    for(int i=0;i<slaveCount - 1;i++){
        rangePairs[i][0] = currentIndex;
        currentIndex += stepLength;
        rangePairs[i][1] = currentIndex;
    }
    //the last node
    rangePairs[slaveCount-1][0] = currentIndex;
    rangePairs[slaveCount-1][1] = INT32_MAX;
}

//why this work correctly?
string myTestTarget(int leadingZeros){
    char target[65];
    for(int i=0;i<leadingZeros;i++)
        target[i] = '0';
    for(int i=leadingZeros;i<64;i++)
        target[i] = '1';
    //c style string end note
    target[64] = '\0';
    //generate c++ style string
    string target_str(target);
    return target_str;
}

void broadcastHash(string hashStr){
    char charBuffer[65];
    strcpy(charBuffer, hashStr.c_str());
    MPI_Bcast(charBuffer, 65, MPI_CHAR, 0, MPI_COMM_WORLD); //65 is the length of the hash + terminater
}

int receiveNonce(){
    int slaveCount;
    MPI_Comm_size(MPI_COMM_WORLD,&slaveCount);
    --slaveCount;
    MPI_Request recv_requests[slaveCount];
    int tempNonce;
    for(int i=0;i<slaveCount;i++){
        MPI_Irecv(&tempNonce, 1, MPI_INT, i+1, 0, MPI_COMM_WORLD, &recv_requests[i]);
    }
    int slaveIndex;
    //wait here
    MPI_Waitany(slaveCount, recv_requests, &slaveIndex, MPI_STATUS_IGNORE);
    return tempNonce;
}

unsigned long long* hexStrTo32bitsNumberSet(const string& bigHexNumber, int &size){
    vector<unsigned char> numberInBytes = *HexToBytes(bigHexNumber);
    size = numberInBytes.size() / 4;
    unsigned long long* numberSet = new unsigned long long [size];
    for (int i = 0, j = 0; i < size; i++, j += 4) {
        numberSet[i] = 
            (numberInBytes[0 + j] << 24 
            | numberInBytes[1 + j] << 16 
            | numberInBytes[2 + j] << 8 
            | numberInBytes[3 + j])
            & 0x00000000ffffffff;
        // cout << std::setw(8) << std::setfill('0') << std::hex << numberSet[i] << std::endl;
    }
    return numberSet;
}

string bigHexNumberMultiple(const string &bigHexNumber, int multiplier) {
    int numberSetSize;
    unsigned long long* numberSet = hexStrTo32bitsNumberSet(bigHexNumber, numberSetSize);

    for(int i=numberSetSize-1, surplus=0; i>=0 ;i--){
        numberSet[i] = numberSet[i] * multiplier + surplus;

        //todo it can be multi-threaded
        if(long long temp = numberSet[i] & 0xffffffff00000000){
            surplus = temp >> 32;
            // cout<< "surpuls: "<< surplus <<std::endl;
            numberSet[i] &= 0x00000000ffffffff;
        }
        // cout << std::setw(8) << std::setfill('0') << std::hex << numberSet[i] << std::endl;
    }
    //todo: deal with last few bytes if any
    // int remainingBytes = numberInBytes.size() % 4;
    std::stringstream output;
    for(int i=0;i<numberSetSize;i++)
        output << std::setw(8) << std::setfill('0') << std::hex << numberSet[i];
    return output.str();
}

string bigHexNumberDivision(const string &bigHexNumber, int divisor){
    int numberSetSize;
    unsigned long long* numberSet = hexStrTo32bitsNumberSet(bigHexNumber, numberSetSize);
    unsigned long long remainder = 0;
    for(int i=0;i<numberSetSize;i++){
        numberSet[i] += remainder<<32;
        remainder = numberSet[i]%divisor;
        numberSet[i] /= divisor;
    }

    //todo: deal with last few bytes if any
    // int remainingBytes = numberInBytes.size() % 4;
    std::stringstream output;
    for(int i=0;i<numberSetSize;i++)
        output << std::setw(8) << std::setfill('0') << std::hex << numberSet[i];
    return output.str();
}