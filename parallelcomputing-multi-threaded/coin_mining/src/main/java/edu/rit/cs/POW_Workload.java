/**
 * POW_Workload (POW - prove of work)
 * @author: Yi Lan
 * The runnable for calculating a nunce
 * 
 * p.s.
 * (POW - prisoner of war (*^_^*)LOL)
 */

package edu.rit.cs;

import java.lang.Runnable;
import java.util.function.Consumer;
import java.util.function.Supplier;

import edu.rit.cs.HashUtil;


public class POW_Workload implements Runnable{
    Object foundFlag;
    String blockHash;
    String targetHash;
    Supplier<Integer> nounce;
    Consumer<String> hashFeedback;

    /**
     * init
     * @param foundFlag - indicate if the hash is found
     * @param blockHash - starting hash
     * @param targetHash
     * @param nonceProvider
     * @param hashOutput
     */
    POW_Workload(Object foundFlag, String blockHash, String targetHash,
     Supplier<Integer> nounceProvider, Consumer<String> hashOutput){
        this.foundFlag = foundFlag;
        this.blockHash = blockHash;
        this.targetHash = targetHash;
        this.nounce = nounceProvider;
        this.hashFeedback = hashOutput;
    }

    @Override
    public void run() {
        String tempHash = blockHash;
        while (targetHash.compareTo(tempHash)<0){
            tempHash = HashUtil.SHA256(HashUtil.SHA256(blockHash+String.valueOf(nounce.get())));
        }
        hashFeedback.accept(tempHash);
        //now owns found flag
        synchronized(foundFlag){
            foundFlag.notifyAll();
        }
    }
}