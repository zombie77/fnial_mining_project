package edu.rit.cs;

import java.util.function.Consumer;
import java.util.function.Supplier;

import edu.rit.cs.*;

public class CoinMining_Multi {
    static Object foundFlag = new Object();    //can this be a Boolean object?
    static Object lock= new Object();
    static int counter[] = {Integer.MIN_VALUE};
    static String resultHash;

    public static void main(String[] args) throws InterruptedException{
        String blockHash = HashUtil.SHA256("CSCI-654 Foundations of Parallel Computing");
        System.out.println("BlockHash: " + blockHash);

        String targetHash = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
        System.out.println("TargetHash: " + targetHash);

        Supplier<Integer> nounceSupplier = ()->{
            int r_value;
            synchronized(lock){
                r_value = counter[0]++;
            }
            return r_value;
        };

        Consumer<String> hashOutput = (String result) -> resultHash = result;

        /* we can make a thread pool to reuse threads and create more jobs if calculation is small */
        int threadCount = 4;
        Thread threadList[] = new Thread[threadCount];
        for(int i=0;i<threadCount;i++){
            threadList[i] = new Thread(new POW_Workload(foundFlag, blockHash, 
                targetHash, nounceSupplier, hashOutput));
        }
        for(Thread t : threadList) t.start();

        //Exception thrown
        synchronized(foundFlag){
            foundFlag.wait();
        }
        //target fount stop all process
        for(Thread t : threadList) t.interrupt();
        System.out.println("the result hash is: " + resultHash);
        System.out.println("the nounce: " + counter[0]);
    }
}
