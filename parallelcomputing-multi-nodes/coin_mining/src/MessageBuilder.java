import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * MessageBuilder
 */
public class MessageBuilder {
    private ByteArrayOutputStream messageBuffer;
    private DataOutputStream out;

    MessageBuilder() {
        messageBuffer = new ByteArrayOutputStream();
        this.out = new DataOutputStream(messageBuffer);
    }

    public MessageBuilder addInteger(Integer... ints) throws IOException {
        for(Integer i : ints){
            out.writeInt(i);
        }
        return this;
    }

    public MessageBuilder addByte(Byte... bytes) throws IOException {
        for(Byte b : bytes){
            out.writeByte(b);
        }
        return this;
    }

    public MessageBuilder addString(String... strs) throws IOException {
        for(String str: strs)
            out.writeBytes(str);
        return this;
    }

    public byte[] getMessage() throws IOException {
        out.flush();
        byte messageBytes[] = messageBuffer.toByteArray();
        messageBuffer.reset();
        return messageBytes;
    }
}