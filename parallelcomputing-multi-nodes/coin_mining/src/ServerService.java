import java.net.*;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.io.*;

public class ServerService {
    // initialize socket and input stream
    // the client - server connection working socket
    private HashMap<String, WorkerSocket> workSocketsMap;
    private ServerSocket serverSocket;
    private OutputStream out;
    private MessageBuilder messageBuilder;

    // TODO: log method to replace the sysout prints
    public ServerService(final int portNum) throws IOException {

        // bind server socket
        serverSocket = new ServerSocket(portNum);
        System.out.println("Server starting");

        // other init invoke
        init();
        System.out.println("Work node server online, waiting for connection");
    }

    private void init() {
        messageBuilder = new MessageBuilder();
    }

    public void close() throws IOException {
        workSocketsMap.forEach((name, socket) -> socket.close());
        serverSocket.close(); //the only thing that's potentially throwing
    }

    /**
     * get connection with all default(none) setting
     * @return
     * @throws AttemptedInvalidConnection
     */
    public String getConnection() throws AttemptedInvalidConnection {
        return getConnection((buff) -> true,
         () -> "socket" + workSocketsMap.size());
    }

    /**
     * overload of getConnection using default connection naming
     */
    public String getConnection(Function<byte[], Boolean> verifier)
     throws AttemptedInvalidConnection {
        return getConnection(verifier, () -> "socket" + workSocketsMap.size());
    }

    /**
     * overload of getConnection without verifier
     * @param nameSupplier
     * @return
     * @throws AttemptedInvalidConnection
     */
    public String getConnection(Supplier<String> nameSupplier)
     throws AttemptedInvalidConnection{
        return getConnection((buff) -> true, nameSupplier);
    }

    /**
     * return a given alias of the connection established
     * @param verifier
     * @param nameSupplier
     * @return
     * @throws AttemptedInvalidConnection
     */
    public String getConnection(Function<byte[], Boolean> verifier, Supplier<String> nameSupplier)
     throws AttemptedInvalidConnection{
        boolean connectionFlag = false;
        String socketAlias = nameSupplier.get();
        WorkerSocket tempSocket = null;
        try {
            tempSocket = new WorkerSocket(serverSocket.accept());
            connectionFlag = verifyConnection(verifier, tempSocket);
            if(connectionFlag){
                //if true establish the connection
                workSocketsMap.put(socketAlias, tempSocket);
            }
            else {
                tempSocket.close();
                throw new AttemptedInvalidConnection();
            }
        } catch (IOException e) {
            //IOexception in socket
            e.printStackTrace();
        }
        return socketAlias;
    }


    /**
     * verify the client
     * 
     * @param verifier method to verify the connection
     * @return
     * @throws IOException if the input stream has failed
     */
    public boolean verifyConnection(Function<byte[], Boolean> verifier, WorkerSocket workSocket) throws IOException {
        DataInputStream in = workSocket.getInputStream();
        byte tempBuffer[] = readInBytes(in);

        //there could be unwanted connections over the network
        //so the verifier basically should behave like a username-password thing
        if (verifier.apply(tempBuffer)) {
            return true;
        } else // deny the connection
            workSocket.close();
        return false;
    }

    /**
     * abandon the given socket by alias
     * @param connectionAlias
     */
    public void abandonConnection(String connectionAlias){
        if(workSocketsMap.containsKey(connectionAlias)){
            workSocketsMap.get(connectionAlias).close();
            workSocketsMap.remove(connectionAlias);
        }
    }

    /**
     * rename a connection
     * @param currentName
     * @param newName
     * @throws Exception
     */
    public void renameConnection(String currentName, String newName) throws Exception {
        if(!workSocketsMap.containsKey(newName)){
            WorkerSocket tempSocket = workSocketsMap.remove(currentName);
            workSocketsMap.put(currentName, tempSocket);
        }
        else throw new Exception("connection" + newName + "already exist\n");
    }

    /**
     * build the binary message
     * @return
     */
    public MessageBuilder getMessageBuilder(){ return messageBuilder;}

    /**
     * send the message to client in plain text
     * @return true if no exception is thrown
     */
    public boolean sendMessage(){
        boolean errorFlag = false;
        try {
            out.write(messageBuilder.getMessage());
        } 
        catch (IOException e) {errorFlag = true;}
        return !errorFlag;
    }

    /**
     * send the message to client in plain text
     * @return true if no exception is thrown
     */
    public boolean sendMessage(Function<byte[], byte[]> encoder){
        boolean errorFlag = false;
        try {
            out.write(encoder.apply(messageBuilder.getMessage()));
        } 
        catch (IOException e) {errorFlag = true;}
        return !errorFlag;
    }

    /**
     * use the provided decoder to create a message for upper layer
     * 
     * this method will get first message from the inputstream
     * if the underlying stream is empty, it will not return until the
     * buffer is not empty
     * 
     * the retrun is in theory never null
     * @param <T>
     * @param decoder
     * @return
     * @throws IOException
     */
    public <T> T getMessage(Function<byte[], T> decoder, String alias) throws IOException {
        if(workSocketsMap.containsKey(alias)){
            byte byteMessage[] = readInBytes(workSocketsMap.get(alias).getInputStream());
            return decoder.apply(byteMessage);
        }
        return null;
    }

    /**
     * read in bytes to a new buffer
     * @param in
     * @return
     * @throws IOException
     */
    private byte[] readInBytes(DataInputStream in) throws IOException {
        int waitTime = 10; //ms
        // loop until the instream has something in buffer
        while (in.available() < 1) { //the return value of available is int so <1 is equivalent to <=0
            try {
                wait(waitTime);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        byte tempBuffer[] = new byte[in.available()];
        in.read(tempBuffer);
        return tempBuffer;
    }
} 

class AttemptedInvalidConnection extends Exception{
    //auto
    private static final long serialVersionUID = 3109216154832119337L;

    @Override
    public String getMessage() {
        String message = super.getMessage() + 
            "faild to verify a connection\n";
        return message;
    }

    //what do java programmer usually put in exceptions?
    //in c++ I usually just throw string
}