import java.io.IOException;
import java.util.BitSet;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * MasterNode
 */
public class MasterNode {
    static final int SERVER_PORT_NUM = 5000;
    ServerService socket;
    Function<byte[], byte[]> encoder;
    Function<byte[], Message> decoder;
    Function<byte[], Boolean> verifier;
    MessageRecreate messageRecreater;
    Map<Integer, String> connectionMap;
    int index[] = {0};//the smallest empty slot

    class NameConnection implements Supplier<String>{
        @Override
        public String get() {
            //find the smallest empty index
            searchEmptySlot();
            String connectionAlias = "Woker" + index[0];
            return connectionMap.put(index[0]++, connectionAlias);
        }

        private void searchEmptySlot(){
            for(;index[0]<connectionMap.size();index[0]++){
                if(!connectionMap.containsKey(index[0]))
                    break;
            }
        }

    }
    
    MasterNode() throws IOException {
        socket = new ServerService(SERVER_PORT_NUM);
        createEncoder();
        createDecoder();
        createVerifier();
        messageRecreater = new MessageRecreate();
        connectionMap = new HashMap<>();
    }

    private void createEncoder(){
        this.encoder = (input) -> {
            BitSet temp = BitSet.valueOf(input);
            temp.flip(0, temp.size());
            return temp.toByteArray();
        };
    }

    private void createDecoder(){
        this.decoder = (input) -> {
            BitSet temp = BitSet.valueOf(input);
            temp.flip(0, temp.size());
            return messageRecreater.makeMessage(temp.toByteArray());
        };
    }

    //it's a verifier accecpts all inputs
    private void createVerifier(){
        this.verifier = (input) -> {
            return true;
        };
    }
}