/**
 * CoinMiner
 */
public class CoinMiner {
    static CoinMiner miner;

    private CoinMiner(){}
    static public CoinMiner getMinerInstance(){
        if(miner == null)
            miner = new CoinMiner();
        return miner;
    }

    public int startMining(String hashValue, String target,
        int nounceLow, int nounceUpper){ return 0;}
    public void stop(){ /* stop all threads */}
    public boolean acceptNounce(String hashValue, String target,
        int indicateNounce){ return false;}
}