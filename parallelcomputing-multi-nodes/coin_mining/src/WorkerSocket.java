import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * WorkerSocket
 */
public class WorkerSocket {
    Socket workerSocket;
    DataInputStream in;
    OutputStream out;
    String ip;

    WorkerSocket(Socket socket) throws IOException {
        workerSocket = socket;
        ip = workerSocket.getInetAddress().getHostAddress();
        in = new DataInputStream(new BufferedInputStream(workerSocket.getInputStream()));
        out = workerSocket.getOutputStream();
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @return the input stream
     */
    public DataInputStream getInputStream() {
        return in;
    }

    /**
     * @return the out
     */
    public OutputStream getOutputStream() {
        return out;
    }

    public void close(){
        try {
            in.close();
            out.close();
            workerSocket.close();
        } catch (Exception e) {
            //TODO: handle exception
            //I never understand why java has this kind of exception
            //can anyone shed some light?
            //why it would thorw IOException? why would we care and how should we handle this type?
        }
    }
}